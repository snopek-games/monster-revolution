extends Node2D

onready var exit_door = $YSort/ExitDoor

func _on_Switch_switch_toggled(switch_position) -> void:
	if switch_position == Switch.SwitchPosition.LEFT:
		exit_door.opened = false
	else:
		exit_door.opened = true

func _on_WinZone_body_entered(body: Node) -> void:
	GameState.win(0)
