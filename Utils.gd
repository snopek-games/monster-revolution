extends Node

signal show_message (message)

func show_message(message: String):
	# A kinda hacky way to get a message shown
	emit_signal("show_message", message)

func get_closest_body(global_position: Vector2, bodies: Array) -> PhysicsBody2D:
	#var space_state = get_world_2d().direct_space_state
	var distance = null
	var closest = null
	for body in bodies:
		var body_distance = global_position.distance_to(body.global_position)
		if distance == null or body_distance < distance:
			#var raycast_result = space_state.intersect_ray(global_position, monster.global_position)
			#if !raycast_result.empty():
			distance = body_distance
			closest = body
	return closest
