extends KinematicBody2D

var Fireball = preload("res://characters/Fireball.tscn")

export (float) var speed = 50.0
export (bool) var frozen := false

onready var initial_scale = scale
onready var health = $Health
onready var hurt_sounds = $HurtSounds.get_children()

var vector := Vector2()
var direction := Vector2.RIGHT

enum State {
	IDLE,
	MOVE,
	ATTACK,
	COOLDOWN,
	HURT,
	DEAD,
}

var state = State.IDLE
var can_attack := true
var monster_detected := false

signal dead

func _physics_process(delta: float) -> void:
	if state == State.IDLE:
		vector = Vector2.ZERO
		$AnimationPlayer.play("Idle")
		if $MoveTimer.is_stopped() and !frozen:
			$MoveTimer.start()
	elif state == State.MOVE:
		$AnimationPlayer.play("Move")
		if $MoveTimer.is_stopped():
			$MoveTimer.start()
	elif state == State.ATTACK:
		vector = Vector2.ZERO
		$AnimationPlayer.play("Idle")
		if can_attack:
			var target_monster = get_closest_monster()
			if target_monster:
				var fireball = Fireball.instance()
				get_parent().add_child(fireball)
				
				direction = ((target_monster.global_position - Vector2(0, 8)) - (global_position - Vector2(0, 8))).normalized()
				update_scale()
				fireball.start((global_position - Vector2(0, 8)) + (direction * 12.0), direction)
				
				# Destroy any fireballs created by dead wizards.
				self.connect("dead", fireball, "queue_free")
				
				can_attack = false
				$CooldownTimer.start()
				
				$Sprite.material.set_shader_param("white", true)
				yield(get_tree().create_timer(0.1), "timeout")
				$Sprite.material.set_shader_param("white", false)
	
	if vector != Vector2.ZERO:
		move_and_slide(vector)

func _on_Hurtbox_area_entered(area: Area2D) -> void:
	if state == State.HURT or state == State.DEAD:
		return
	
	state = State.HURT

	var damage = area.get("damage")
	if damage == null:
		damage = 1
	health.health -= damage

	var rebound_vector = area.get("rebound_vector")
	if rebound_vector and rebound_vector != Vector2.ZERO:
		vector = rebound_vector
	else:
		vector = direction * -200.0
	
	$AnimationPlayer.play("Hurt")
	
	hurt_sounds[randi() % hurt_sounds.size()].play()

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == 'Hurt':
		if health.health == 0:
			state = State.DEAD
			emit_signal("dead")
			$AnimationPlayer.play("Dead")
		elif monster_detected:
			state = State.ATTACK
		else:
			state = State.IDLE
	elif anim_name == 'Dead':
		queue_free()

func update_scale() -> void:
	if direction.x < 0:
		scale.x = -initial_scale.x * sign(scale.y)
	else:
		scale.x = initial_scale.x * sign(scale.y)

func _on_MoveTimer_timeout() -> void:
	if state == State.IDLE:
		state = State.MOVE
		direction = Vector2(randf() - 0.5, randf() - 0.5).normalized()
		vector = direction * speed
		update_scale()
		
	elif state == State.MOVE:
		state = State.IDLE

func get_closest_monster():
	var distance = null
	var closest = null
	for body in $MonsterDetectionArea.get_overlapping_bodies():
		if body.captive:
			continue
		var body_distance = global_position.distance_to(body.global_position)
		if distance == null or body_distance < distance:
			distance = body_distance
			closest = body
	return closest

func _on_MonsterDetectionArea_body_entered(body: Node) -> void:
	if state == State.HURT or state == State.DEAD:
		return
	monster_detected = true
	state = State.ATTACK

func _on_MonsterDetectionArea_body_exited(body: Node) -> void:
	if state == State.HURT or state == State.DEAD:
		return
	if $MonsterDetectionArea.get_overlapping_bodies().size() == 0:
		monster_detected = false
	state = State.IDLE

func _on_CooldownTimer_timeout() -> void:
	can_attack = true

func _on_AnimationPlayer_animation_changed(old_name: String, new_name: String) -> void:
	# Attempt to fix issue where color gets stuck.
	if old_name == 'Hurt':
		modulate = Color(1.0, 1.0, 1.0, 1.0)
