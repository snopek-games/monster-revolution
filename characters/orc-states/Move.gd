extends "res://characters/orc-states/Idle.gd"

onready var move_timer = $MoveTimer

var vector := Vector2.ZERO
var min_attack_distance := 25

func _state_enter(info : Dictionary):
	host.animation_player.play("Move")
	if info.has('input_vector'):
		do_move(info['input_vector'])
		if info.get("immediately", false):
			host.move_and_slide(vector)
	
	if !host.player_controlled:
		move_timer.wait_time = (randf() * 2.0) + 0.5
		move_timer.start()

func _state_exit() -> void:
	move_timer.stop()

func _physics_process(delta):
	if host.player_controlled:
		var input_vector = _get_player_input_vector()
		if !_check_for_player_attack_input(input_vector) and input_vector == Vector2.ZERO:
			get_parent().change_state("Idle")
		else:
			do_move(input_vector)
			host.move_and_slide_with_push(vector)
	else:
		# If in attack mode, and we detect an enemy, either attack or move
		# towards them.
		if GameState.minion_mode == GameState.MinionMode.ATTACK:
			var enemy = _detect_enemy()
			if enemy:
				var enemy_vector = (enemy.global_position - host.global_position).normalized()
				if enemy.global_position.distance_to(host.global_position) < min_attack_distance:
					get_parent().change_state("Attack", {"input_vector": enemy_vector})
				else:
					do_move(enemy_vector)
					host.move_and_slide(vector)
				return
		
		# If the player is detected, in either mode, then attempt to follow them.
		var player = _detect_player()
		if player:
			do_move((player.global_position - host.global_position).normalized())
			
		host.move_and_slide_with_push(vector)

func do_move(input_vector: Vector2):
	host.direction = input_vector
	# TODO: accelerate to top speed
	vector = input_vector * host.speed

func _on_MoveTimer_timeout() -> void:
	if get_parent().current_state == self:
		get_parent().change_state("Idle")
