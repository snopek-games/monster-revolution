extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."
onready var cooldown_timer = $CooldownTimer

var vector := Vector2.ZERO

func _state_enter(info: Dictionary) -> void:
	# TODO: accerate to rebound speed
	vector = -host.direction * host.rebound_speed
	cooldown_timer.start()

func _state_exit() -> void:
	cooldown_timer.stop()

func _physics_process(delta: float) -> void:
	host.move_and_slide(vector)

func _on_CooldownTimer_timeout() -> void:
	if get_parent().current_state != self:
		return
	
	get_parent().change_state("Idle")
