extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _state_enter(info: Dictionary) -> void:
	host.animation_player.play("Dead")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if get_parent().current_state != self:
		return
	
	if anim_name == "Dead":
		if GameState.player == host:
			GameState.player = null
		host.queue_free()
