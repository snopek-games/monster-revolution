extends CanvasLayer

onready var hud = $HUD
onready var fade = $Fade
onready var controller = $OnScreenController

var screens = []

func _ready() -> void:
	for child in get_children():
		if child.name.ends_with("Screen"):
			screens.append(child)

func hide_screen() -> void:
	for screen in screens:
		screen.visible = false

func show_screen(name: String, info: Dictionary = {}) -> void:
	if name.ends_with("Screen"):
		hide_screen()
		var screen: Control = get_node(name)
		if screen:
			screen.visible = true
			hide_hud()
			if screen.has_method("show_screen"):
				screen.show_screen(info)

func show_hud() -> void:
	hud.visible = true
	if OS.has_touchscreen_ui_hint():
		show_controller()

func hide_hud() -> void:
	hud.visible = false
	if OS.has_touchscreen_ui_hint():
		hide_controller()

func show_controller() -> void:
	controller.visible = true

func hide_controller() -> void:
	controller.visible = false
