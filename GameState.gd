extends Node

var player setget set_player

enum MinionMode {
	FOLLOW = 0,
	ATTACK = 1,
	STAY = 2,
	MAX = 3,
}

const DEFAULT_MINION_MODE = MinionMode.FOLLOW

var minion_mode: int = DEFAULT_MINION_MODE setget set_minion_mode

signal player_set (player)
signal player_removed (player)
signal minion_mode_changed (minion_mode)
signal win (monster_count)

func set_player(_player):
	if player != _player:
		if player != null:
			emit_signal("player_removed", player)
		player = _player
		if player != null:
			emit_signal("player_set", player)
			self.minion_mode = DEFAULT_MINION_MODE

func set_minion_mode(_minion_mode):
	if minion_mode != _minion_mode:
		minion_mode = _minion_mode
		emit_signal("minion_mode_changed", minion_mode)

func win(monster_count: int) -> void:
	emit_signal("win", monster_count)
