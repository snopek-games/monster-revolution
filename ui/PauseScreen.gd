extends Control

signal continue_pressed
signal controls_pressed
signal quit_pressed

func _ready() -> void:
	if OS.has_touchscreen_ui_hint():
		$HBoxContainer/ControlsButton.visible = false

func show_screen(info: Dictionary) -> void:
	$HBoxContainer/ContinueButton.grab_focus()

func _on_ContinueButton_pressed() -> void:
	emit_signal("continue_pressed")

func _on_ControlsButton_pressed() -> void:
	emit_signal("controls_pressed")

func _on_QuitButton_pressed() -> void:
	emit_signal("quit_pressed")
