extends Control

onready var continue_button = $HBoxContainer/ContinueButton

signal continue_pressed

func show_screen(info: Dictionary) -> void:
	$Text.text = info['message']
	$HBoxContainer/ContinueButton.grab_focus()

func _on_ContinueButton_pressed() -> void:
	emit_signal("continue_pressed")
