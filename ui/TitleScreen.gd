extends Control

signal play
signal credits
signal quit

func _ready() -> void:
	var os_name = OS.get_name()
	# Hide the Quit button on platforms where it doesn't make sense.
	if os_name == "Android" or os_name == "iOS" or os_name == 'HTML5':
		$HBoxContainer/QuitButton.visible = false

func show_screen(info: Dictionary) -> void:
	$HBoxContainer/PlayButton.grab_focus()

func _on_PlayButton_pressed() -> void:
	emit_signal("play")

func _on_CreditsButton_pressed() -> void:
	emit_signal("credits")

func _on_QuitButton_pressed() -> void:
	emit_signal("quit")
