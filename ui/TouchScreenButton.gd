extends TouchScreenButton

onready var up_modulate: Color = modulate
export (Color) var down_modulate := Color(1.0, 1.0, 1.0, 0.9)

func _on_pressed() -> void:
	modulate = down_modulate

func _on_released() -> void:
	modulate = up_modulate
