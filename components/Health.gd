extends Node

export (int) var max_health := 6 setget set_max_health
export (int) var health := 6 setget set_health

signal max_health_changed (max_health)
signal health_changed (health)

func set_max_health(_max_health: int) -> void:
	if max_health != _max_health:
		max_health = _max_health
		if max_health < 1:
			max_health = 1
		emit_signal("max_health_changed", max_health)
		if health > max_health:
			self.health = max_health

func set_health(_health: int) -> void:
	if health != _health:
		health = clamp(_health, 0, max_health)
		emit_signal("health_changed", health)
