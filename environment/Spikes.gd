tool
extends "res://components/Hitbox.gd"

class_name Spikes

export (bool) var spikes_activated := false setget set_spikes_activated

signal spikes_toggled (spikes_activated)

func set_spikes_activated(_activated: bool) -> void:
	if spikes_activated != _activated:
		spikes_activated = _activated
		if spikes_activated:
			$AnimationPlayer.play("Activate")
			$SpikesActivatedSound.play()
		else:
			$SpikesDeactivatedSound.play()
			$AnimationPlayer.play_backwards("Activate")
		emit_signal("spikes_toggled", spikes_activated)

func toggle_spikes() -> void:
	set_spikes_activated(!spikes_activated)

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	$CollisionShape2D.set_deferred("disabled", !spikes_activated)
