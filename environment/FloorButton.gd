tool
extends Area2D

class_name FloorButton

enum ButtonColor {
	RED,
	BLUE
}

export (ButtonColor) var color := ButtonColor.RED setget set_color
export (bool) var pressed := false setget set_pressed

signal button_toggled (pressed)

func set_color(_color):
	color = _color
	_update_frame()

func set_pressed(_pressed: bool):
	if pressed != _pressed:
		pressed = _pressed
		$CollisionShape2D.set_deferred("disabled", pressed)
		_update_frame()
		$SwitchSound.play()
		emit_signal("button_toggled", pressed)
		if get_tree():
			$Sprite.material.set_shader_param("white", true)
			yield(get_tree().create_timer(0.1), "timeout")
			$Sprite.material.set_shader_param("white", false)

func _update_frame() -> void:
	var frame = 2 if color == ButtonColor.BLUE else 0
	frame += 1 if pressed else 0
	$Sprite.frame = frame

func _on_Button_body_entered(body: Node) -> void:
	set_pressed(true)
