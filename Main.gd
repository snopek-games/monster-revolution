extends Node2D

onready var ui_layer = $UILayer
onready var fade = ui_layer.fade
onready var camera = $Camera2D
onready var initial_camera_position = camera.global_position

onready var click_sound_effect = $ClickSoundEffect
onready var music = $Music

export (PackedScene) var main_map_scene
export (bool) var skip_title_screens := false
export (bool) var create_invincible_player := false

var Orc = preload("res://characters/Orc.tscn")

var title_map_scene = preload('res://maps/TitleMap.tscn')
var playing := false

func _ready() -> void:
	var os_name = OS.get_name()
	print ("OS name: " + os_name)
	if os_name == "Android" or os_name == "iOS":
		# On mobile, expand to fit the whole screen.
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_EXPAND, Vector2(320, 180), 1.0)
	
	GameState.connect("player_set", self, "_on_GameState_player_set")
	GameState.connect("player_removed", self, "_on_GameState_player_removed")
	GameState.connect("win", self, "_on_GameState_win")
	Utils.connect("show_message", self, "_on_Utils_show_message")
	
	show_title_screen()

func _on_GameState_player_set(player) -> void:
	player.health.connect("health_changed", self, "_on_Player_health_changed")
	
func _on_GameState_player_removed(player) -> void:
	player.health.disconnect("health_changed", self, "_on_Player_health_changed")

func show_title_screen() -> void:
	if skip_title_screens:
		load_main_map()
		return
	
	music.play("TitleMusic")
	camera.global_position = initial_camera_position
	camera.call_deferred("reset_smoothing")
	ui_layer.show_screen("TitleScreen")
	load_map(title_map_scene, false)

func load_main_map() -> void:
	playing = true
	music.play("MainMapMusic")
	ui_layer.hide_screen()
	ui_layer.show_hud()
	load_map(main_map_scene)
	fade.fade_in()

func play_click_sound() -> void:
	click_sound_effect.play()

func _on_TitleScreen_play() -> void:
	play_click_sound()
	$Map.open_door()
	yield(get_tree().create_timer(0.5), "timeout")
	fade.fade_out()
	yield(fade, "fade_out_finished")
	$Map.visible = false
	ui_layer.show_screen("LoreScreen")
	music.play("MainMapMusic")
	fade.fade_in()

func _on_LoreScreen_continue_pressed() -> void:
	play_click_sound()
	
	if OS.has_touchscreen_ui_hint():
		_on_ControlsScreen_continue_pressed()
	else:
		ui_layer.show_screen("ControlsScreen")

func _on_ControlsScreen_continue_pressed() -> void:
	play_click_sound()
	fade.fade_out()
	yield(fade, "fade_out_finished")
		
	if playing:
		$Map.visible = true
		ui_layer.show_screen("PauseScreen")
		fade.fade_in()
		yield(fade, "fade_in_finished")
	else:
		load_main_map()

func _on_TitleScreen_credits() -> void:
	play_click_sound()
	fade.fade_out()
	yield(fade, "fade_out_finished")
	$Map.visible = false
	ui_layer.show_screen("CreditsScreen")
	fade.fade_in()

func _on_CreditsScreen_back() -> void:
	play_click_sound()
	fade.fade_out()
	yield(fade, "fade_out_finished")
	$Map.visible = true
	ui_layer.show_screen("TitleScreen")
	fade.fade_in()

func _on_TitleScreen_quit() -> void:
	get_tree().quit()

func _input(event: InputEvent) -> void:
	if playing and $Map.visible:
		if get_tree().paused:
			if event.is_action_pressed("pause") or event.is_action_pressed("ui_cancel"):
				play_click_sound()
				do_unpause()
		else:
			# For some reason, all the buttons except the Start button are 
			# recognized when using the Retroflag GPi.
			var gpi_start_button = (OS.get_name() == "Unix" && event is InputEventJoypadButton && event.button_index == 7)
			if event.is_action_pressed("pause") or gpi_start_button:
				play_click_sound()
				get_tree().paused = true
				ui_layer.show_screen("PauseScreen")

func do_unpause():
	ui_layer.hide_screen()
	ui_layer.show_hud()
	get_tree().paused = false

func _on_PauseScreen_continue_pressed() -> void:
	play_click_sound()
	do_unpause()

func _on_PauseScreen_controls_pressed() -> void:
	play_click_sound()
	fade.fade_out()
	yield(fade, "fade_out_finished")
	$Map.visible = false
	ui_layer.show_screen("ControlsScreen")
	fade.fade_in()
	yield(fade, "fade_in_finished")

func _on_PauseScreen_quit_pressed() -> void:
	play_click_sound()
	playing = false
	fade.fade_out()
	yield(fade, "fade_out_finished")
	show_title_screen()
	get_tree().paused = false
	fade.fade_in()

func _on_Utils_show_message(message: String) -> void:
	playing = false
	get_tree().paused = true
	ui_layer.show_screen("MessageScreen", {message = message})

func _on_MessageScreen_continue_pressed() -> void:
	ui_layer.hide_screen()
	ui_layer.show_hud()
	playing = true
	get_tree().paused = false

func _on_Player_health_changed(health: int) -> void:
	if health == 0:
		playing = false
		yield(get_tree().create_timer(1.0), "timeout")
		ui_layer.show_screen("FailScreen")
		music.play("LoseMusic")

func _on_GameState_win(monster_count: int) -> void:
	playing = false
	fade.fade_out()
	yield(fade, "fade_out_finished")
	$Map.visible = false
	ui_layer.show_screen("WinScreen", {monster_count = monster_count})
	music.play("WinMusic")
	fade.fade_in()

func _on_HUD_mode_changed() -> void:
	play_click_sound()

func load_map(map_scene: PackedScene, create_player: bool = true) -> void:
	if has_node("Map"):
		var old_map = get_node("Map")
		remove_child(old_map)
		old_map.queue_free()
		
	var map = map_scene.instance()
	map.name = 'Map'
	map.pause_mode = Node.PAUSE_MODE_STOP
	add_child(map)
	
	if create_player:
		var player = Orc.instance()
		player.name = "Player"
		player.orc_appearance = player.Appearance.PAINTED
		player.player_controlled = true
		player.captive = false
		player.invincible = create_invincible_player
		
		var camera_path
		if map.has_node('YSort'):
			map.get_node('YSort').add_child(player)
			camera_path = @"../../../../Camera2D"
		else:
			map.add_child(player)
			camera_path = @"../../../Camera2D"
		
		if map.has_node('PlayerStartPosition'):
			player.global_position = map.get_node('PlayerStartPosition').global_position
		
		camera.global_position = player.global_position
		camera.call_deferred("reset_smoothing")
		
		var remote_transform = RemoteTransform2D.new()
		player.add_child(remote_transform)
		remote_transform.remote_path = camera_path
		
		GameState.player = player
	else:
		GameState.player = null

